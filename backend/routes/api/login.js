 module.exports = function(app, passport){
app.get('/api/login', function(req, res) {
        res.render('login'); 
    });
 app.post('/api/login', passport.authenticate('signup', {
        successRedirect : '/', 
        failureRedirect : '/login', 
        failureFlash : true 
    }));
 };