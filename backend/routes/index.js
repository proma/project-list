var checkToken = require('../middleware/checkToken.js');
var routes = require('./api/routes');

var User = require('../schemas/userSchema');
var userRepository = require('../repositories/UserRepository');
module.exports = function(app , passport){
	//app.use(isLoggedIn);
	
	app.get('/',  function(req, res){
		res.render('login');
/*		User.findOne({
			email: req.decoded.email
		}).exec(function(err, user){
			if (err){
				throw err;
			}
			if(!user){
				userRepository.add(req.decoded , function(err ,data){
					res.data = data;
					res.err = err;
					next();
				});
			}

		});
	});*/

	routes(app);
});
};
function isLoggedIn(req, res, next) {

    if (req.isAuthenticated())
        return next();

    res.redirect('/login');
}